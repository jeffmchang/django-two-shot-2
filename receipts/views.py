from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.
def receipt_list(request):
    list = Receipt.objects.all()

    context = {"list": list}

    return render(request, "receipts/list.html", context)
